interface Rectangle {
    width:number;
    height:number;


}

interface ColorRectangle extends Rectangle{
    color:string;
}

const rectangle: Rectangle = {
        width: 20,
        height:20
}
console.log(rectangle);

const colorRectangle :ColorRectangle={
    width : 20,
    height : 20,
    color : "Red"
}

console.log(colorRectangle);