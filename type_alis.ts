type CarYear =number;
type CarModel = string;
type CarType = string;

type Car = {
    year:CarYear,
    model:CarModel,
    type:CarType,
}

const carYear: CarYear = 2001;
const carType: CarType = "Toyota";
const carModel: CarModel = "Corola";

const car1:Car = {
    year:2001,
    model:"XXX",
    type:"Nissan"
}

console.log(car1);